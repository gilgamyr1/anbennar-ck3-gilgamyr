﻿
k_verne = {
	1000.1.1 = { change_development_level = 8 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0002	#Rocair Vernid
	}
}

k_menibor = {
	1000.1.1 = { change_development_level = 10 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0004	#Maurisio sil Menibor
	}
}

k_armanhal = {
	1000.1.1 = { change_development_level = 10 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0005	#Emil, King of Armanhal
	}
}


d_verne = {
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0002	#Rocair Vernid
	}
}

c_verne = {
	1000.1.1 = { change_development_level = 10 }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = vernid_0003	#Arman Vernid
	}
	989.6.30 = {
		holder = vernid_0002	#Rocair Vernid
	}
}

d_the_tail = {
	1000.1.1 = { change_development_level = 6 }
	500.1.1 = {
		liege = k_verne
	}
	
	969.2.10 = {
		holder = tails_end_0001
	}
}

c_the_tail = {
	969.2.10 = {
		holder = tails_end_0001
	}
}

c_stingport = {
	1000.1.1 = { change_development_level = 8 }
	
	969.2.10 = {
		holder = tails_end_0001
	}
	990.10.12 = {	#Sorncost-Pearlsedge join the war and make base at Stingport. Pearlsedge refuses to give it back!
		liege = k_pearlsedge
		holder = pearlman_0011 # Serek "Seaborn" Pearlman
	}
}

c_lokkex_heights = {
	969.2.10 = {
		holder = tails_end_0001
	}
}

d_wyvernmark = {
	1000.1.1 = { change_development_level = 7 }
	500.1.1 = {
		liege = k_verne
	}
	
	970.4.20 = {
		holder = bronzewing_0001
	}
	999.12.29 = {
		holder = bronzewing_0003
	}
}

c_wyvernmark = {
	970.4.20 = {
		holder = bronzewing_0001
	}
	999.12.29 = {
		holder = bronzewing_0003
	}
}

c_arca_lar = {
	970.4.20 = {
		holder = bronzewing_0001
	}
	999.12.29 = {
		holder = bronzewing_0003
	}
}

c_bronzewing = {
	970.4.20 = {
		holder = bronzewing_0001
	}
	999.12.29 = {
		holder = bronzewing_0003
	}
}

c_armanhal = {
	1000.1.1 = { change_development_level = 10 }
}

d_menibor_loop = {
	1000.1.1 = { change_development_level = 10 }
}

c_menibor = {
	1000.1.1 = { change_development_level = 17 }
}

c_napesbay = {
	1000.1.1 = { change_development_level = 9 }
}

d_galeinn = {
	1000.1.1 = { change_development_level = 9 }
}

c_walterton = {
	1000.1.1 = { change_development_level = 9 }
}

c_bellacaire = {
	1000.1.1 = { change_development_level = 16 }
}